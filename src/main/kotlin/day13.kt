fun main() {
    val input = readInputLines("data/day13.txt")

    print("part1: ")
    d13p1(input)

    print("part2:\n")
    d13p2(input)
}

private fun d13p1(input: List<String>) {
    val tp = TransparentPaper(input)
    tp.doFold(0)

    println(tp.points.size)
}

private fun d13p2(input: List<String>) {
    val tp = TransparentPaper(input)
    (0 until tp.folds.size).forEach { tp.doFold(it) }

    println(tp.toString())
}

private class TransparentPaper(input: List<String>) {
    val points: MutableSet<Point> = mutableSetOf()
    val folds: MutableList<String> = mutableListOf()

    init {
        val iter = input.iterator()

        do {
            val line = iter.next()
            if (line.isNotEmpty()) {
                val split = line.split(',')
                assert(split.size == 2)

                points.add(Point(split[0].toInt(), split[1].toInt()))
            }
        } while (line.isNotEmpty())

        folds.addAll(iter.asSequence())
    }

    fun doFold(i: Int) {
        val fold = folds[i]

        val foldInstruction = fold.split('=')

        val foldPos = foldInstruction[1].toInt()

        val newPoints =
            if (foldInstruction[0] == "fold along y") {
                points.map {
                    if (it.row > foldPos) {
                        Point(it.col, 2 * foldPos - it.row)
                    } else {
                        it
                    }
                }
            } else {
                points.map {
                    if (it.col > foldPos) {
                        Point(2 * foldPos - it.col, it.row)
                    } else {
                        it
                    }
                }
            }.toList()

        points.clear()
        points.addAll(newPoints)
    }

    override fun toString(): String {
        val maxCol = this.points.maxOf { it.col }
        val maxRow = this.points.maxOf { it.row }

        val s = StringBuffer("")

        for (row in 0..maxRow) {
            for (col in 0..maxCol) {
                if (points.contains(Point(col, row))) {
                    s.append("#")
                } else {
                    s.append(" ")
                }
            }
            s.append("\n")
        }

        return s.toString()
    }
}
