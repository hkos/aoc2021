fun main() {
    val input = readInputLines("data/day10.txt")

    print("part1: ")
    d10p1(input)

    print("part2: ")
    d10p2(input)
}

private fun d10p1(input: List<String>) {
    val lines = navLines(input)

    var value = 0

    for (line in lines) {
        try {
            line.missing()
        } catch (ic: IllegalChar) {
            // This is the case we're interested in, for part 1
            val badChar = ic.message!![0]
            value += illegalValue[badChar]!!
        } finally {
            // Ignore incomplete lines
        }
    }

    println(value)
}


private fun d10p2(input: List<String>) {
    val lines = navLines(input)

    val scores: MutableList<Long> = mutableListOf()

    for (line in lines) {
        try {
            val fix = line.missing()
            if (fix.isNotEmpty()) {
                val score: Long = fix.fold(0) { acc, it ->
                    acc * 5 + fixValue(it)
                }

                scores.add(score)
            }
        } catch (e: Exception) {
            // Ignore corrupted lines
        }
    }

    scores.sort()

    // odd number of scores expected!
    assert(scores.size % 2 == 1)

    val middle = (scores.size - 1) / 2
    println(scores[middle])
}

// NOTE: "char" must be one character long
class IllegalChar(char: String) : Exception(char)

private val chunk = mapOf<Char, Char>(
    Pair('(', ')'),
    Pair('[', ']'),
    Pair('{', '}'),
    Pair('<', '>'),
)

private val illegalValue = mapOf<Char, Int>(
    Pair(')', 3),
    Pair(']', 57),
    Pair('}', 1197),
    Pair('>', 25137),
)

private fun fixValue(char: Char): Long =
    when (char) {
        ')' -> 1
        ']' -> 2
        '}' -> 3
        '>' -> 4
        else -> throw  Exception(
            "unexpected char in fix: $char"
        )
    }


private fun isOpening(char: Char) = chunk.keys.contains(char)
private fun isClosing(char: Char) = chunk.values.contains(char)

// find the entry for 'char' as a closing symbol
private fun entryFor(char: Char) =
    chunk.entries.find { it.value == char }

private class NavLine(val input: String) {
    // Returns:
    // - IllegalChar Exception if illegal char found,
    // - empty List if all is fine,
    // - list of necessary fixes if line is incomplete
    fun missing(): List<Char> {
        val stack: MutableList<Char> = mutableListOf()

        for (char in input.toCharArray()) {
            if (isOpening(char)) {
                stack.add(char)
            } else if (isClosing(char)) {
                val last = stack.removeLast()
                if (last != entryFor(char)!!.key) {
                    throw IllegalChar("$char")
                }
            } else {
                throw Exception("unexpected character $char")
            }
        }

        // calculate missing completion characters
        return stack.reversed().map { chunk[it]!! }.toList()
    }

}

private fun navLines(input: List<String>) =
    input.map { NavLine(it) }.toList()
