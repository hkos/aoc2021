fun main() {
    val input = readInputLines("data/day14.txt")

    print("part1: ")
    d14p1(input)

    print("part2: ")
    d14p2(input)
}

private fun d14p1(input: List<String>) {
    val poly = Polymer(input)

    (1..10).forEach { _ -> poly.step() }
    println(poly.mostMinusLeast())
}

private fun d14p2(input: List<String>) {
    val poly = Polymer(input)

    (1..40).forEach { _ -> poly.step() }
    println(poly.mostMinusLeast())
}

private data class PPair(val a: Char, val b: Char)

private class Polymer(input: List<String>) {
    val template: String
    val rules: HashMap<PPair, Char>
    var pairs: MutableMap<PPair, Long>

    fun addPairs(map: MutableMap<PPair, Long>, pp: PPair, n: Long) {
        map[pp] = map.getOrDefault(pp, 0) + n
    }

    fun insertion(pp: PPair): Pair<PPair, PPair> {
        val c = rules[pp]!!
        return Pair(PPair(pp.a, c), PPair(c, pp.b))
    }

    init {
        template = input[0]

        pairs = mutableMapOf()
        template.windowed(2).map {
            val (a, b) = it.toCharArray()
            PPair(a, b)
        }.forEach { addPairs(pairs, it, 1) }

        rules = hashMapOf()
        for (i in 2 until input.size) {
            val c = input[i].toCharArray()
            rules[PPair(c[0], c[1])] = c[6]
        }
    }

    fun step() {
        val new = mutableMapOf<PPair, Long>()
        pairs.forEach {
            val (a, b) = insertion(it.key)
            addPairs(new, a, it.value)
            addPairs(new, b, it.value)
        }

        pairs = new
    }

    private fun counts(): Map<Char, Long> {
        val counts = mutableMapOf<Char, Long>()

        fun add(c: Char, n: Long) {
            counts[c] = counts.getOrDefault(c, 0) + n
        }

        pairs.forEach {
            add(it.key.a, it.value)
            add(it.key.b, it.value)
        }

        // Adding up each element of each PPair counts every element twice,
        // except for the very first and last ond.
        // To compensate, we add 1 for the "first" and "last" char from
        // 'template' (these remain unchanged after each step).
        add(template.first(), 1)
        add(template.last(), 1)

        // Then divide all counts by 2
        counts.forEach { counts[it.key] = counts[it.key]!! / 2 }

        return counts.toMap()
    }

    fun mostMinusLeast(): Long {
        val sortedCounts = counts().map { it.value }.sorted().toList()
        return sortedCounts.last() - sortedCounts.first()
    }
}
