fun main() {
    val input = readInputLines("data/day16.txt")

    print("part1: ")
    d16p1(input)

    print("part2: ")
    d16p2(input)
}

private fun d16p1(input: List<String>) {
    val bits = Bits(input[0])

    val packets = parsePackets(bits.iterator(), 1)
    println(packets[0].versionSum())
}

private fun d16p2(input: List<String>) {
    val bits = Bits(input[0])

    val packets = parsePackets(bits.iterator(), 1)
    println(packets[0].value())
}

private abstract class Packet(val version: Byte) {
    abstract fun versionSum(): Int
    abstract fun value(): Long
}

private class LiteralPacket(version: Byte, input: Iterator<Boolean>) :
    Packet(version) {

    val bits: List<Boolean>

    init {
        bits = mutableListOf()
        do {
            val group = input.asSequence().take(5).iterator()

            val isFinal = !group.next()
            group.forEach { bits.add(it) }
        } while (!isFinal)
    }

    override fun versionSum() = version.toInt()
    override fun value() = toLong(bits.asSequence())

    override fun toString() =
        "Literal (v$version) [${toLong(this.bits.asSequence())}]"
}

private class OpPacket(
    version: Byte, val type: Byte, input:
    Iterator<Boolean>
) : Packet(version) {

    val subPackets: List<Packet>

    init {
        subPackets = if (input.next()) {
            // Number of sub-packets immediately contained by this packet
            val numPackets = toLong(input.asSequence().take(11)).toInt()

            parsePackets(input, numPackets)
        } else {
            // Total length in bits of the sub-packets in this packet
            val lenBits = toLong(input.asSequence().take(15)).toInt()

            parsePackets(
                input.asSequence().take(lenBits).iterator(),
                null
            )
        }
    }

    override fun versionSum() =
        version.toInt() + subPackets.sumOf { it.versionSum() }

    override fun value() =
        when (type.toInt()) {
            0 -> {
                subPackets.sumOf { it.value() }
            }
            1 -> {
                subPackets.map { it.value() }
                    .fold(1.toLong()) { acc, it -> acc * it }
            }
            2 -> {
                subPackets.minOf { it.value() }
            }
            3 -> {
                subPackets.maxOf { it.value() }
            }
            5 -> {
                if (subPackets[0].value() > subPackets[1].value()) 1 else 0
            }
            6 -> {
                if (subPackets[0].value() < subPackets[1].value()) 1 else 0
            }
            7 -> {
                if (subPackets[0].value() == subPackets[1].value()) 1 else 0
            }
            else -> throw Exception("op $type not implemented")
        }

    override fun toString() = "Op (v$version) $type $subPackets"
}

private fun parsePackets(
    iterator: Iterator<Boolean>,
    limit: Int?
): List<Packet> {
    val packets = mutableListOf<Packet>()

    while ((limit == null || packets.size < limit) && iterator.hasNext()) {
        val version = toLong(iterator.asSequence().take(3)).toByte()
        val type = toLong(iterator.asSequence().take(3)).toByte()

        packets.add(
            when (type) {
                4.toByte() -> LiteralPacket(version, iterator)
                else -> OpPacket(version, type, iterator)
            }
        )
    }

    return packets
}

fun toLong(seq: Sequence<Boolean>) =
    seq.fold(0.toLong()) { acc, it -> acc * 2 + if (it) 1 else 0 }

private class Bits(input: String) {
    private val bits = hexToBinary(input)

    private fun hexToBinary(hex: String): Sequence<Boolean> =
        hex.toCharArray().asSequence()
            .map { "$it".toInt(16) }
            .flatMap {
                listOf(
                    it.and(0x8) != 0,
                    it.and(0x4) != 0,
                    it.and(0x2) != 0,
                    it.and(0x1) != 0,
                )
            }.asSequence()

    fun iterator() = bits.iterator()
}
