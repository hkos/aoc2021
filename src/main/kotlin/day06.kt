fun main() {
    val lines = readInputLines("data/day06.txt")

    print("part1: ")
    d6p1(lines)

    print("part2: ")
    d6p2(lines)
}

private fun d6p1(input: List<String>) {
    val sea = Sea(input[0])
    sea.run(80)

    println(sea.numFish())
}

private fun d6p2(input: List<String>) {
    val sea = Sea(input[0])
    sea.run(256)

    println(sea.numFish())
}

class Sea(input: String) {
    // map: counter -> how many fish with this counter are there
    private var fish: HashMap<Int, Long> = hashMapOf()

    init {
        input.split(',')
            .forEach { addWithCounter(it.toInt()) }
    }

    private fun getByCounter(counter: Int): Long =
        fish.getOrDefault(counter, 0)

    private fun addWithCounter(counter: Int) {
        fish[counter] = getByCounter(counter) + 1
    }

    fun run(steps: Int) =
        (1..steps).forEach { _ -> step() }

    private fun step() {
        val newState = hashMapOf<Int, Long>()

        // Handle fish that simply decrement their counter
        for (counter in 1..8) {
            newState[counter - 1] = getByCounter(counter)
        }

        // Handle spawning fish (with counter=0)
        val spawningFish = getByCounter(0)
        // 1) add new fish
        newState[8] = spawningFish
        // 2) re-add the spawning fish with counter=6
        newState[6] = newState.getOrDefault(6, 0) + spawningFish

        // Replace state of the sea with this new state
        fish = newState
    }

    fun numFish(): Long = fish.values.sum()

}
