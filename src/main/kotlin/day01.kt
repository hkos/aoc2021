import java.io.File

fun main(args: Array<String>) {
    val nums = readInputNums("data/day01.txt")

    print("part1: ")
    part1(nums)

    print("part2: ")
    part2(nums)
}

fun part1(nums: List<Int>) {
    var increases=0;

    for (i in 0 until nums.size-1) {
        if (nums[i] < nums[i+1]) {
            increases++;
        }
    }

    println(increases)
}

fun part2(nums: List<Int>) {
    var increases=0;

    for (i in 0 until nums.size-3) {
        val a =nums[i]+nums[i+1]+nums[i+2]
        val b =nums[i+1]+nums[i+2]+nums[i+3]

        if (a < b) {
            increases++;
        }
    }

    println(increases)
}

fun readInputNums(fileName: String): List<Int> {
    val nums: MutableList<Int> = mutableListOf()

    File(fileName).forEachLine {
        nums.add(it.toInt())
    }

    return nums
}