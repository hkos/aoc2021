import kotlin.system.measureTimeMillis

fun main() {
    val input = readInputLines("data/day15.txt")

    print("part1: ")
    d15p1(input)

    print("part2: ")
    d15p2(input)
}

private fun d15p1(input: List<String>) {
    val cave = Cavern(input)

    val time = measureTimeMillis {
        val short = cave.searchShortest()
        println(short)
    }

    println("took $time ms")
}

private fun d15p2(input: List<String>) {
    val mega = MegaCave(input)

    val time = measureTimeMillis {
        val short = mega.searchShortest()
        println(short)
    }

    println("took $time ms")
}

private class MegaCave(input: List<String>) : Cavern(
    Array(input.size * 5)
    { row ->
        val srcRow = row % input.size
        val rowRepeat = row / input.size

        (0 until input[0].length * 5)
            .map { col ->
                val srcCol = col % input[0].length
                val colRepeat = col / input[0].length

                val v =
                    input[srcRow][srcCol].digitToInt() + rowRepeat + colRepeat

                if (v > 9) v - 9 else v
            }
            .toList()
            .toIntArray()
    }
)

private open class Cavern : NumericalMap {
    val idealPathTo = hashMapOf<Point, Int>()
    var best = Int.MAX_VALUE

    constructor(array: Array<IntArray>) : super(array)
    constructor(input: List<String>) : super(input)

    fun searchShortest() =
        descend(
            Point(0, 0),
            Point(width() - 1, height() - 1),
            0
        )!!

    private fun descend(pos: Point, goal: Point, len: Int): Int? {
        if (len > best) {
            // we already spent too much length - give up on this branch
            return null
        }

        if (idealPathTo[pos] != null && idealPathTo[pos]!! <= len) {
            // we've already explored a better path that traverses this field
            return null
        } else {
            idealPathTo[pos] = len
        }

        return if (pos == goal) {
            // arrived at goal

            // new best path?
            if (len < best) {
                println("found new best path, len $len")
                best = len
            }

            len
        } else {
            pos
                .neighbors()
                .filter { it.row in 0..goal.row && it.col in 0..goal.col }
                .sortedBy { -it.row - it.col } // prioritize right/down
                .mapNotNull { descend(it, goal, len + value(it)!!) }
                .minOrNull()
        }
    }

}
