fun main() {
    val input = readInputLines("data/day11.txt")

    print("part1: ")
    d11p1(input)

    print("part2: ")
    d11p2(input)
}

private fun d11p1(input: List<String>) {
    val el = EnergyLevels(input)

    val sum = (1..100).sumOf { el.step() }
    println(sum)
}

private fun d11p2(input: List<String>) {
    val map = EnergyLevels(input)

    for (step in 1..Int.MAX_VALUE) {
        val flashes = map.step()
        if (flashes == 100) {
            println(step)
            break
        }
    }
}

private class EnergyLevels(input: List<String>) : NumericalMap(input) {
    fun step(): Int {
        // a list of all points in this map
        val all = allPoints()

        // First, the energy level of each octopus increases by 1.
        all.forEach { set(it, value(it)!! + 1) }

        // all octopi that have already been determined to flash in this step
        val flashed: MutableList<Point> = mutableListOf()

        // newly flashing octopi (that are not yet listed in "flashed")
        fun flash() =
            all
                .filter { value(it)!! > 9 }
                .filter { it !in flashed }

        do {
            val newFlash = flash()

            newFlash.forEach {
                flashed.add(it)

                // This increases the energy level of all adjacent
                // octopuses by 1, including octopuses that are diagonally
                // adjacent.
                it.allNeighbors().forEach { n ->
                    val value = value(n)
                    if (value != null) {
                        set(n, value + 1)
                    }
                }
            }

            // If this causes an octopus to have an energy
            // level greater than 9, it also flashes. This process
            // continues as long as new octopuses keep having their
            // energy level increased beyond 9.
            // (An octopus can only flash at most once per step.)

        } while (newFlash.isNotEmpty())

        // Finally, any octopus that flashed during this step has
        // its energy level set to 0, as it used all of its energy
        // to flash.
        flashed.forEach { set(it, 0) }

        return flashed.size
    }
}
