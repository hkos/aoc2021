fun main() {
    val lines = readInputLines("data/day05.txt")

    print("part1: ")
    d5p1(lines)

    print("part2: ")
    d5p2(lines)
}

fun d5p1(input: List<String>) {
    val lines = loadLines(input)
    val counts: HashMap<Pair<Int, Int>, Int> = hashMapOf()

    for (line in lines) {
        // for now, we ignore diagonals
        val points = line.points(false)

        for (p in points) {
            val v = counts.getOrDefault(p, 0)
            counts.put(p, v + 1)
        }
    }

    var x = 0
    for (count in counts.values) {
        if (count >= 2) {
            x++
        }
    }

    println(x)
}

fun d5p2(input: List<String>) {
    val lines = loadLines(input)

    val counts: HashMap<Pair<Int, Int>, Int> = hashMapOf()

    for (line in lines) {
        // for now, we ignore diagonals
        val points = line.points(true)

        for (p in points) {
            val v = counts.getOrDefault(p, 0)
            counts.put(p, v + 1)
        }
    }

    var x = 0
    for (count in counts.values) {
        if (count >= 2) {
            x++
        }
    }

    println(x)
}

fun loadLines(input: List<String>): List<Line> {
    val lines: MutableList<Line> = mutableListOf()

    for (i in input) {
        val l = Line(i)
        lines.add(l)
    }

    return lines.toList()
}

class Line(input: String) {
    private val x1: Int
    private val x2: Int
    private val y1: Int
    private val y2: Int

    init {
        val points = input.split(" -> ")

        val p0 = point(points[0])
        val p1 = point(points[1])

        x1 = p0.first
        x2 = p1.first
        y1 = p0.second
        y2 = p1.second

    }

    private fun point(p: String): Pair<Int, Int> {
        val x = p.split(',')
        return Pair(x[0].toInt(), x[1].toInt())
    }

    private fun delta(a: Int, b: Int): Int {
        return if (a == b) {
            0
        } else if (a < b) {
            1
        } else {
            -1
        }
    }

    fun points(diag: Boolean): List<Pair<Int, Int>> {
        val points: MutableList<Pair<Int, Int>> = mutableListOf()

        val dx = delta(x1, x2)
        val dy = delta(y1, y2)

        if (!diag && (dx != 0 && dy != 0)) {
            // don't handle diagonals
            return listOf()
        }

        var x = x1
        var y = y1
        points.add(Pair(x, y))

        while (!((x2 == x) && (y2 == y))) {
            x += dx
            y += dy

            points.add(Pair(x, y))
        }

        return points.toList()
    }
}
