fun main() {
    val input = readInputLines("data/day09.txt")

    print("part1: ")
    d9p1(input)

    print("part2: ")
    d9p2(input)
}

private fun d9p1(input: List<String>) {
    val hm = HeightMap(input)

    val risk = hm.lowPoints().sumOf { hm.value(it)!! + 1 }
    println(risk)
}


private fun d9p2(input: List<String>) {
    val basins = HeightMap(input).basins()

    val sizes = basins.map { it.size }.toList().sorted()
    val mult = sizes.reversed().take(3).fold(1) { acc, it -> acc * it }

    println(mult)
}


data class Point(val col: Int, val row: Int) {
    fun neighbors(): List<Point> =
        listOf(
            Point(col, row - 1),
            Point(col, row + 1),
            Point(col - 1, row),
            Point(col + 1, row),
        )

    // including diagonals
    fun allNeighbors(): List<Point> =
        listOf(
            Point(col, row - 1),
            Point(col, row + 1),
            Point(col - 1, row),
            Point(col + 1, row),

            Point(col - 1, row - 1),
            Point(col + 1, row + 1),
            Point(col - 1, row + 1),
            Point(col + 1, row - 1),
        )
}


open class NumericalMap {
    private var map: Array<IntArray>

    constructor(input: List<String>) {
        map = Array(input.size) { line ->
            input[line]
                .toCharArray()
                .asSequence()
                .map { it.digitToInt() }
                .toList()
                .toIntArray()
        }
    }

    constructor(map: Array<IntArray>) {
        this.map = map
    }

    fun value(p: Point): Int? = map.getOrNull(p.row)?.getOrNull(p.col)

    fun set(p: Point, value: Int) =
        map.getOrNull(p.row)?.set(p.col, value)

    fun allPoints(): List<Point> =
        map.withIndex()
            .flatMap { (y, line) -> line.indices.map { x -> Point(x, y) } }
            .toList()

    fun height() = map.size
    fun width() = map[0].size
}

private class HeightMap(input: List<String>) : NumericalMap(input) {
    fun lowPoints() = allPoints().filter { isLowPoint(it) }

    // p is a low point if the heights of all neighbors are bigger that p's
    private fun isLowPoint(p: Point) =
        p.neighbors().mapNotNull { value(it) }.all { it > value(p)!! }

    fun basins(): List<List<Point>> {
        // A list of Basins (each Basin is represented as a List<Point>)
        val basins: MutableList<List<Point>> = mutableListOf()

        // All Points on the map that haven't been identified as part of a
        // basin yet. We will leave all '9'-valued Points in the list,
        // since they don't belong to any basin..
        val unused = allPoints().toMutableList()

        fun nonNine() = unused.filter { value(it) != 9 }

        while (nonNine().isNotEmpty()) {
            val basin = mutableListOf<Point>()

            val first = nonNine().first()
            val new: MutableList<Point> = mutableListOf(first)
            unused.remove(first)

            while (new.isNotEmpty()) {
                // process the first entry in 'new'
                val n = new.removeFirst()

                basin.add(n)

                n.neighbors()
                    .filter { it in unused }
                    .filter { value(it) != 9 }
                    .forEach() {
                        // this point belongs to the current basin
                        new.add(it)
                        unused.remove(it)
                    }
            }

            basins.add(basin.toList())
        }

        return basins.toList()
    }
}
