import javax.swing.Painter

fun main(args: Array<String>) {
    val lines = readInputLines("data/day04.txt")

    print("part1: ")
    d4p1(lines)

    print("part2: ")
    d4p2(lines)
}

fun d4p1(lines: List<String>) {
    val input = loadBoards(lines)
    val rand = input.first
    val boards = input.second

    val scores = scores(rand, boards);

    var bestSteps: Int? = null
    var bestScore = 0

    for (s in scores) {
        val steps = s.first
        val score = s.second

        if (bestSteps == null || bestSteps!! > steps) {
            bestSteps = steps
            bestScore = score
        }
    }

    println("(winning score) $bestScore")
}


fun d4p2(lines: List<String>) {
    val input = loadBoards(lines)
    val rand = input.first
    val boards = input.second

    val scores = scores(rand, boards);

    var worstSteps = 0
    var thatScore = 0

    for (s in scores) {
        val steps = s.first
        val score = s.second

        if (worstSteps < steps) {
            worstSteps = steps
            thatScore = score
        }
    }

    println("(losing score) $thatScore")
}

fun scores(rand: Random, boards: List<Board>): List<Pair<Int, Int>> {
    var scores: MutableList<Pair<Int, Int>> = mutableListOf();

    for (b in boards) {
        val res = b.play(rand)

        val steps = res.first!!
        val number: Int = res.second!!

        val unmark = b.sumUnmarked()
        val score = number * unmark

        scores.add(Pair(steps, score))
    }
    return scores.toList()
}

fun loadBoards(lines: List<String>): Pair<Random, List<Board>> {
    val rand = Random(lines[0])
    var boards: MutableList<Board> = mutableListOf()

    for (i in 2..lines.size step 6) {
        val b = Board(lines.subList(i, i + 5))

        boards.add(b)
    }
    return Pair(rand, boards)
}

class Random(line: String) {
    private var r: MutableList<Int> = mutableListOf()

    init {
        for (num in line.split(",")) {
            r.add(num.toInt())
        }
    }

    fun asList(): List<Int> {
        return r.toList()
    }
}

class Board(lines: List<String>) {
    private var fields = Array(5) { Array(5) { 0 } }
    private var marked = Array(5) { Array(5) { false } }

    init {
        for ((i, line) in lines.withIndex()) {
            for ((j, field) in line.trim().split(Regex("\\ +")).withIndex()) {
                fields[i][j] = field.toInt()
            }
        }
    }

    fun sumUnmarked(): Int {
        var sum = 0
        for (line in 0 until 5) {
            for (col in 0 until 5) {
                if (!marked[line][col]) {
                    sum += fields[line][col]
                }

            }
        }

        return sum
    }

    // Returns how many random numbers were used for win and the winning
    // number, if any (both null if no win)
    fun play(rand: Random): Pair<Int?, Int?> {
        for ((i, r) in rand.asList().withIndex()) {
            mark(r)
            if (bingo()) {
                return Pair(i + 1, r)
            }
        }

        return Pair(null, null)
    }

    fun mark(num: Int): Boolean {
        for (line in 0 until 5) {
            for (col in 0 until 5) {
                if (fields[line][col] == num) {
                    marked[line][col] = true
                    return true
                }

            }
        }

        return false
    }

    fun bingo(): Boolean {
        for (i in 0 until 5) {
            if (bingo_line(i) || bingo_col(i)) {
                return true
            }
        }
        return false
    }

    private fun bingo_line(line: Int): Boolean {
        for (col in 0 until 5) {
            if (!marked[line][col]) {
                return false
            }
        }
        return true
    }

    private fun bingo_col(col: Int): Boolean {
        for (line in 0 until 5) {
            if (!marked[line][col]) {
                return false
            }
        }
        return true
    }

}