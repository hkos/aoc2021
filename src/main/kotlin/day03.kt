fun main(args: Array<String>) {
    val lines = readInputLines("data/day03.txt")

    print("part1: ")
    d3p1(lines)

    print("part2: ")
    d3p2(lines)
}

fun d3p1(lines: List<String>) {
    val gamma = calc(lines, true)
    val eps = calc(lines, false)

    println(gamma * eps)
}

fun d3p2(lines: List<String>) {
    val oxy = oxy(lines, 0)
    val co2 = co2(lines, 0)

    println(oxy * co2)
}

fun oxy(lines: List<String>, pos: Int): Int {
    val most = mostCommon(lines, pos)

    val lines = filterBy(lines, pos, most)
    if (lines.size == 1) {
        // make base10, return
        return Integer.parseInt(lines[0], 2)
    } else {
        return oxy(lines, pos + 1)
    }
}

fun co2(lines: List<String>, pos: Int): Int {
    val least = leastCommon(lines, pos)

    val lines = filterBy(lines, pos, least)
    if (lines.size == 1) {
        // make base10, return
        return Integer.parseInt(lines[0], 2)
    } else {
        return co2(lines, pos + 1)
    }
}

fun filterBy(lines: List<String>, pos: Int, keep: Int): List<String> {
    val filtered: MutableList<String> = mutableListOf()
    val comp = if (keep == 0) {
        '0'
    } else {
        '1'
    }

    for (line in lines) {
        if (line.get(pos) == comp) {
            filtered.add(line)
        }
    }

    return filtered
}

fun calc(lines: List<String>, gamma: Boolean): Int {
    var num = 0

    for (pos in 0 until lines[0].length) {
        val most = mostCommon(lines, pos)

        num *= 2
        if (gamma && most == 1) {
            num++
        } else if (!gamma && most == 0) {
            num++
        }
    }

    return num
}

fun count(lines: List<String>, pos: Int): Pair<Int, Int> {
    var ones = 0
    var zeros = 0
    for (line in lines) {
        when (line.get(pos)) {
            '0' -> zeros++
            '1' -> ones++
            else -> {
                throw Exception("Unexpected digit in `$line`.")
            }
        }
    }

    return Pair(zeros, ones)
}

fun mostCommon(lines: List<String>, pos: Int): Int {
    val count = count(lines, pos)

    val zeros = count.first
    val ones = count.second

    if (ones >= zeros) {
        return 1
    } else {
        return 0
    }
}

fun leastCommon(lines: List<String>, pos: Int): Int {
    val count = count(lines, pos)

    val zeros = count.first
    val ones = count.second

    if (ones < zeros) {
        return 1
    } else {
        return 0
    }
}
