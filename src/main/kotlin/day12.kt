fun main() {
    val input = readInputLines("data/day12.txt")

    print("part1: ")
    d12p1(input)

    print("part2: ")
    d12p2(input)
}

private fun d12p1(input: List<String>) {
    val map = CaveMap(input)

    val paths = map.paths(false)
    println(paths.size)
}

private fun d12p2(input: List<String>) {
    val map = CaveMap(input)

    val paths = map.paths(true)
    println(paths.size)
}


private class CaveMap(input: List<String>) {
    val conn: Map<String, List<String>>

    init {
        val c: MutableMap<String, List<String>> = mutableMapOf()

        fun add(a: String, b: String) {
            val value = c.getOrDefault(a, listOf()).toMutableList()
            value.add(b)
            c[a] = value
        }

        for (line in input) {
            val foo = line.split('-')
            assert(foo.size == 2)

            // add both directions to list of connections
            add(foo[0], foo[1])
            add(foo[1], foo[0])
        }

        conn = c.toMap()
    }

    class Path {
        var p: MutableList<String>

        var doubleSmallUsed = false

        constructor(step: String) {
            p = mutableListOf(step)
        }

        private constructor(path: MutableList<String>) {
            p = path
        }

        fun add(step: String): Path {
            val p2 = p.toList().toMutableList()
            p2.add(step)

            val x = Path(p2)
            x.doubleSmallUsed = this.doubleSmallUsed
            return x
        }
    }

    fun paths(doubleSmall: Boolean): List<Path> =
        descend(listOf(Path("start")), doubleSmall)


    private fun descend(foo: List<Path>, doubleSmall: Boolean): List<Path> {
        // 'foo' are paths explored so far, not all reaching to 'end' yet

        // 'bar' will be the new state after attempting to take one step for
        // each entry in foo
        val bar: MutableList<Path> = mutableListOf()

        // if any changes were made, switch this to true
        var changes = false


        for (f in foo) {
            // for each f in foo, try to make one more step from the final step
            // (unless the final step is already 'end'; just keep these paths).

            val last = f.p.last()
            if (last == "end") {
                bar.add(f)
                continue
            }

            for (next in conn[last]!!) {

                // return: <legal move, is double small>
                fun canDo(next: String): Pair<Boolean, Boolean> {
                    if (!next.toCharArray().all { it.isLowerCase() }) {
                        // not a lowercase room -> ok multiple times
                        return Pair(true, false)
                    }

                    // This is a "double use" case
                    if (f.p.contains(next)) {

                        // double start or end are never legal
                        if (next == "start" || next == "end") {
                            return Pair(false, true)
                        }

                        // If double small is allowed, check if the current
                        // path 'f' has already used up this option.
                        return if (doubleSmall && !f.doubleSmallUsed) {
                            Pair(true, true)
                        } else {
                            Pair(false, true)
                        }

                    }

                    // Lowercase cave that we haven't visited yet
                    return Pair(true, false)
                }

                // don't consider paths that have visit small caves twice
                // (i.e.: if the step leads us to a small room, and the path
                // already contains this room, the step is illegal)
                //
                // [unless 'doubleSmall' is "true": then we are allowed to
                // visit one small cave twice. If so, we remember this in
                // Path.doubleSmallUsed]
                val (allowed, double) = canDo(next)
                if (!allowed) {
                    continue
                }

                val p2 = f.add(next)
                bar.add(p2)

                if (double) {
                    p2.doubleSmallUsed = true
                }

                changes = true
            }


        }

        // descend again, unless all paths already ended on 'end'
        return if (changes) {
            descend(bar, doubleSmall)
        } else {
            // no more changes -> we're done
            bar.toList()
        }
    }
}
