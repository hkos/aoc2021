fun main() {
    val input = readInputLines("data/day18.txt")

    print("part1: ")
    d18p1(input)

    print("part2: ")
    d18p2(input)
}

private fun d18p1(input: List<String>) {
    val iter = input.map { snailNumOf(it) }.iterator()

    var value = iter.next()

    while (iter.hasNext()) {
        value = value.add(iter.next()).reduce()
    }

    println(value.magnitude())
}

private fun d18p2(input: List<String>) {
    val numbers = input.map { snailNumOf(it) }.toSet()

    val max = numbers.flatMap { a ->
        numbers.minus(a).map { b -> a.add(b).reduce().magnitude() }
    }.maxOrNull()

    println(max)
}

private fun snailNumOf(s: String): SnailNum {
    val (sn, remaining) = recurse(s.substring(1, s.length - 1))
    assert(remaining.isEmpty())
    return sn
}

// FIXME: use char iterator?
private fun recurse(s: String): Pair<SnailNum, String> {
    var remaining = s

    val a: SnailNum
    val b: SnailNum

    if (remaining.first().isDigit()) {
        a = SimpleSnailNum(remaining.first().digitToInt())
        remaining = remaining.substring(1)
    } else if (remaining.first() == '[') {
        val x = recurse(remaining.substring(1))
        a = x.first
        remaining = x.second

        assert(remaining.first() == ']')
        remaining = remaining.substring(1)
    } else {
        throw Exception("unexpected rest $remaining")
    }

    assert(remaining.first() == ',')
    remaining = remaining.substring(1)

    if (remaining.first().isDigit()) {
        b = SimpleSnailNum(remaining.first().digitToInt())
        remaining = remaining.substring(1)
    } else if (remaining.first() == '[') {
        val x = recurse(remaining.substring(1))
        b = x.first
        remaining = x.second

        assert(remaining.first() == ']')
        remaining = remaining.substring(1)
    } else {
        throw Exception("unexpected rest $remaining")
    }

    return Pair(PairSnailNum(a, b), remaining)
}


private abstract class SnailNum {
    fun add(other: SnailNum) = PairSnailNum(this.copy(), other.copy())

    fun reduce(): SnailNum {

        // return: Pair( "just exploded" [I need to be replaced with 0] , "need to add Int to the right" )
        fun explodeX(psn: PairSnailNum, depth: Int, prevSimple: SimpleSnailNum?): Pair<Boolean, Int?> {
            if (depth > 4) {
                if (prevSimple != null) {
                    prevSimple.i += (psn.a as SimpleSnailNum).i
                }

                // need to add b to next SimpleVal
                return Pair(true, (psn.b as SimpleSnailNum).i)
            }

            if (psn.a is PairSnailNum) {
                val (explode, b) = explodeX(psn.a as PairSnailNum, depth + 1, prevSimple)
                if (b != null) {
                    if (explode) {
                        psn.a = SimpleSnailNum(0)
                    }

                    val left = psn.b.leftmost()
                    left.i += b
                }
                if (explode) {
                    return Pair(true, null)
                }
            }

            if (psn.b is PairSnailNum) {
                val (explode, b) = explodeX(psn.b as PairSnailNum, depth + 1, psn.a.rightmost())

                if (b != null) {
                    if (explode) {
                        psn.b = SimpleSnailNum(0)
                    }
                    return Pair(false, b) // see if someone upstream has a place to add this to
                }

                if (explode) {
                    return Pair(true, null)
                }
            }

            return Pair(false, null) // Not exploded: no value needs to be added to the right.
        }


        fun explode(): Boolean {
            // If any pair is nested inside four pairs, the leftmost such pair explodes:

            // To explode a pair, the pair's left value is added to the first regular number to the left of the
            // exploding pair (if any), and the pair's right value is added to the first regular number to the
            // right of the exploding pair (if any). Exploding pairs will always consist of two regular numbers.
            // Then, the entire exploding pair is replaced with the regular number 0.

            if (this is PairSnailNum) {
                return explodeX(this, 1, null).first
            } else {
                throw Exception("Unexpected top level snail num")
            }
        }

        fun splitFrom(i: Int) = PairSnailNum(SimpleSnailNum(i / 2), SimpleSnailNum((i + 1) / 2))

        fun splitX(psn: PairSnailNum): Boolean {
            if (psn.a is SimpleSnailNum && (psn.a as SimpleSnailNum).i >= 10) {
                psn.a = splitFrom((psn.a as SimpleSnailNum).i)
                return true
            } else if (psn.a is PairSnailNum) {
                if (splitX(psn.a as PairSnailNum)) {
                    return true
                }
            }

            if (psn.b is SimpleSnailNum && (psn.b as SimpleSnailNum).i >= 10) {
                psn.b = splitFrom((psn.b as SimpleSnailNum).i)
                return true
            } else if (psn.b is PairSnailNum) {
                if (splitX(psn.b as PairSnailNum)) {
                    return true
                }
            }

            return false
        }

        fun split(): Boolean {
            // If any regular number is 10 or greater, the leftmost such regular number splits.

            // To split a regular number, replace it with a pair; the left element of the pair should be the
            // regular number divided by two and rounded down, while the right element of the pair should be the
            // regular number divided by two and rounded up. For example, 10 becomes [5,5], 11 becomes [5,6],
            // 12 becomes [6,6], and so on.

            if (this is PairSnailNum) {
                return splitX(this)
            } else {
                throw Exception("Unexpected top level snail num")
            }
        }

        do {
            val change = explode() || split()
        } while (change)

        return this
    }

    abstract fun magnitude(): Int
    abstract fun leftmost(): SimpleSnailNum
    abstract fun rightmost(): SimpleSnailNum
    abstract fun copy(): SnailNum
}

private data class SimpleSnailNum(var i: Int) : SnailNum() {
    override fun magnitude() = i
    override fun leftmost() = this
    override fun rightmost() = this
    override fun copy() = SimpleSnailNum(this.i)
    override fun toString() = "$i"
}

private data class PairSnailNum(var a: SnailNum, var b: SnailNum) : SnailNum() {
    override fun magnitude() = 3 * a.magnitude() + 2 * b.magnitude()
    override fun leftmost() = this.a.leftmost()
    override fun rightmost() = this.b.rightmost()
    override fun copy() = PairSnailNum(this.a.copy(), this.b.copy())
    override fun toString() = "[$a,$b]"
}
