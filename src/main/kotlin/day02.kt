import java.io.File

fun main(args: Array<String>) {
    val lines = readInputLines("data/day02.txt")

    print("part1: ")
    d2p1(lines)

    print("part2: ")
    d2p2(lines)
}

fun d2p1(lines: List<String>) {
    var depth = 0
    var pos = 0

    for (line in lines) {
        val parts = line.split(" ")
        val cmd = parts[0]
        val n = parts[1].toInt()

        when (cmd) {
            "forward" -> pos += n
            "down" -> depth += n
            "up" -> depth -= n
            else -> {
                throw Exception("Unexpected command `$cmd`.")
            }
        }
    }

    println(depth * pos)
}

fun d2p2(lines: List<String>) {
    var aim = 0
    var pos = 0
    var depth = 0

    for (line in lines) {
        val parts = line.split(" ")
        val cmd = parts[0]
        val n = parts[1].toInt()

        when (cmd) {
            "forward" -> {
                pos += n
                depth += aim * n
            }
            "down" -> aim += n
            "up" -> aim -= n
            else -> {
                throw Exception("Unexpected command `$cmd`.")
            }
        }
    }

    println(depth * pos)
}

fun readInputLines(fileName: String): List<String> {
    val lines: MutableList<String> = mutableListOf()

    File(fileName).forEachLine {
        lines.add(it)
    }

    return lines
}