import kotlin.math.max

fun main() {
    val input = readInputLines("data/day17.txt")

    print("part1: ")
    d17p1(input)

    print("part2: ")
    d17p2(input)
}

private fun d17p1(input: List<String>) {
    val probe = Probe(input[0])

    // HACK: manually guessed "sensible" range for dx/dy
    val best =
        (1..100).flatMap { dx ->
            (1..200).mapNotNull { dy -> probe.launch(dx, dy) }
        }.maxOrNull()

    println(best)
}

private fun d17p2(input: List<String>) {
    val probe = Probe(input[0])

    // HACK: manually guessed "sensible" range for dx/dy
    val count =
        (1..100).flatMap { dx ->
            (-200..200).mapNotNull { dy -> probe.launch(dx, dy) }
        }.count()
    
    println(count)
}

private class Probe(input: String) {
    var minX: Int = 0
    var maxX: Int = 0
    var minY: Int = 0
    var maxY: Int = 0

    init {
        val pattern =
            "^target area: x=(\\d+)..(\\d+), y=(-?\\d+)..(-?\\d+)$".toRegex()

        pattern.findAll(input).forEach { matchResult ->
            minX = matchResult.groupValues[1].toInt()
            maxX = matchResult.groupValues[2].toInt()
            minY = matchResult.groupValues[3].toInt()
            maxY = matchResult.groupValues[4].toInt()
        }
    }

    // Launch the probe.
    // Returns the maximum height reached, if the target is reached,
    // null otherwise.
    fun launch(dx: Int, dy: Int): Int? {
        var pos = Pair(0, 0)
        var delta = Pair(dx, dy)

        fun overshot() = pos.second < minY || pos.first > maxX

        fun move() {
            pos = Pair(
                pos.first + delta.first,
                pos.second + delta.second
            )
        }

        fun updateDelta() {
            delta = Pair(max(0, delta.first - 1), delta.second - 1)
        }

        fun inTarget() = pos.first in minX..maxX && pos.second in minY..maxY

        var maxY = 0

        while (!overshot()) {
            move()
            if (pos.second > maxY) maxY = pos.second

            // Reached target -> return max height
            if (inTarget()) return maxY

            updateDelta()
        }

        // Missed the target
        return null
    }
}
