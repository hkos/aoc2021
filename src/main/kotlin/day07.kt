import kotlin.math.absoluteValue
import kotlin.math.min

fun main() {
    val lines = readInputLines("data/day07.txt")

    print("part1: ")
    d7p1(lines[0])

    print("part2: ")
    d7p2(lines[0])
}

private fun d7p1(input: String) {
    val crabs = Crabs(input)

    fun costLinear(a: Int, b: Int): Int =
        (a - b).absoluteValue

    println(crabs.minFuel(::costLinear))
}

private fun d7p2(input: String) {
    val crabs = Crabs(input)

    fun costTriangular(a: Int, b: Int): Int {
        val n = (a - b).absoluteValue

        // https://en.wikipedia.org/wiki/Triangular_number
        return (n * (n + 1)) / 2
    }

    println(crabs.minFuel(::costTriangular))
}

class Crabs(input: String) {
    private val pos: List<Int> = input
        .split(',')
        .map { it.toInt() }
        .toList()

    fun minFuel(costFn: (a: Int, b: Int) -> Int): Int =
        posRange().minOf { fuelToTarget(it, costFn) }

    private fun fuelToTarget(
        target: Int,
        costFn: (Int, Int) -> Int
    ): Int =
        pos.sumOf { costFn(it, target) }

    private fun posRange(): IntRange =
        pos.minOrNull()!!..pos.maxOrNull()!!

}
