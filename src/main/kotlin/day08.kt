fun main() {
    val lines = readInputLines("data/day08.txt")

    print("part1: ")
    d8p1(lines)

    print("part2: ")
    d8p2(lines)
}

private fun d8p1(input: List<String>) {
    val entries = input.map { Display(it) }.toList()

    val count = entries.sumOf { entry ->
        entry.outputs().count { it.length in listOf(2, 3, 4, 7) }
    }

    println(count)
}

private fun d8p2(input: List<String>) {
    val entries = input.map { Display(it) }.toList()

    println(entries.sumOf { it.outputValue() })
}


class Display(input: String) {
    private val patterns: List<String>
    private val outputs: List<String>

    // The char in this display that corresponds to the "real" e segment
    private val segmentE: Char

    // The encoding of the digit one, in this display
    private val digitOne: String

    init {
        val parts = input.split(" | ")

        patterns = parts[0].split(' ')
        outputs = parts[1].split(' ')

        // Find the digit "1", which is the only one with 2 segments
        digitOne = patterns.find { it.length == 2 }!!

        // The real segment `e` is the only one that comes up in 4 patterns.
        // What is that segment called in this display?
        segmentE = ('a'..'g').find { char ->
            patterns.count { char in it } == 4
        }!!
    }

    // List of all output patterns
    fun outputs(): List<String> = outputs

    // Value of all output digits, interpreted as a number
    fun outputValue() =
        outputs.fold(0) { acc, pattern -> 10 * acc + patternValue(pattern) }

    // Does this pattern contain both segments of the digit one?
    private fun containsOneDigits(pattern: String) =
        pattern.contains(digitOne[0]) && pattern.contains(digitOne[1])

    // The value of a particular pattern, for this display
    private fun patternValue(pattern: String) =
        when (pattern.length) {
            2 -> 1
            3 -> 7
            7 -> 8
            4 -> 4
            5 -> {
                if (containsOneDigits(pattern)) {
                    3  // 5 digits + contains "1"
                } else
                    if (pattern.contains(segmentE)) {
                        2 // 5 digits + doesn't contain "1" + has 'real' e
                    } else {
                        5 // 5 digits + doesn't contain "1" + has no 'real' e
                    }
            }
            6 -> {
                if (containsOneDigits(pattern)) {
                    if (pattern.contains(segmentE)) {
                        0 // 6 digits + contains "1" + has 'real' e
                    } else {
                        9 // 6 digits + contains "1" + has no 'real' e
                    }
                } else {
                    6 // 6 digits + doesn't contain "1"
                }
            }
            else -> throw Exception("Unexpected pattern length")
        }

}